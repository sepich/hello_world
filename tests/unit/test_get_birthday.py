import json
from datetime import date, timedelta

from app import app


def test_get_birthday():
    today = date.today()
    ret = app.get_birthday('John', today.month, today.day)
    assert ret["statusCode"] == 200
    data = json.loads(ret["body"])
    assert data["message"] == "Hello, John! Happy birthday!"

    in_5d = today + timedelta(days=5)
    ret = app.get_birthday('John', in_5d.month, in_5d.day)
    assert ret["statusCode"] == 200
    data = json.loads(ret["body"])
    assert data["message"] == "Hello, John! Your birthday is in 5 days"
