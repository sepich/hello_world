import json
import os
import pymysql
from datetime import date, datetime
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def response(status, msg=None):
    '''Log response and return json'''
    res = {'statusCode': status, 'headers': {'Content-Type': 'application/json'}}
    if msg:
        res['body'] = json.dumps({'message': msg})
    logger.info(json.dumps({
        'status': status,
        'message': msg
    }))
    return res


def update_birthday(name, payload):
    '''Update user birthday'''
    try:
        birthday = json.loads(payload)['dateOfBirth']
        dt = datetime.strptime(birthday, '%Y-%m-%d')  # checks for leap year
    except:
        return response(400, 'Wrong payload')

    sql = '''INSERT INTO birthdays
        SET name=%(name)s, year=%(year)s, month=%(month)s, day=%(day)s, created=NOW(), modified=NOW()
        ON DUPLICATE KEY UPDATE year=%(year)s, month=%(month)s, day=%(day)s, modified=NOW()'''
    with db.cursor() as cursor:
        cursor.execute(sql, {'name': name, 'year': dt.year, 'month': dt.month, 'day': dt.day})
        db.commit()
    return response(204)


def get_birthday(name, month, day):
    '''Get days till birthday'''
    today = date.today()
    year = today.year
    if today.month == month and today.day == day:
        return response(200, f'Hello, {name}! Happy birthday!')
    elif today.month > month or (today.month == month and today.day > day):
        year += 1  # birthday in the next year

    try:
        birthday = date(year, month, day)
    except ValueError:
        birthday = date(year, month, day - 1)  # leap year
    days = (birthday - today).days
    return response(200, f'Hello, {name}! Your birthday is in {days} days')


def lambda_handler(event, context):
    '''API gateway request handler'''
    try:
        name = event['pathParameters']['name']
        if event['httpMethod'] == 'PUT':
            return update_birthday(name, event['body'])

        elif event['httpMethod'] == 'GET':
            with db.cursor() as cursor:
                cursor.execute('SELECT `month`, `day` FROM `birthdays` WHERE `name`=%s', (name,))
                result = cursor.fetchone()
            if not result:
                return response(404)
            return get_birthday(name, result['month'], result['day'])
    except:
        logger.exception("Handler exception:")
        return response(500)


if __name__ == "app":
    # warm-up lambda
    stack = os.getenv('STACK')
    logger.info(f'Starting in {stack} mode...')
    with open(f'{stack}.json') as f:
        conf = json.load(f)
    db = pymysql.connect(**conf, charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
