# Design

Looking at the task definition, first thing coming to my mind is classic container microservices. Something like Application Load Balancer and Fargate to start.  
There is not much of details about expected load and performance requirements. I assume it is a new service with unknown future. API seems to be a draft (`Name` is bad UID), so need to do simplest possible MVP. And then iterate on data from real usage.  
As it is "not required to support both AWS and GCP" – simplest solution I see is to use Lambda here:

![](https://habrastorage.org/webt/pd/ni/tg/pdnitguxoow7rvxzhemxc4jrc1c.png)

 * Lambda - concentrate on product development, not infra. Each Lambda instance serves only single request at a time, so no need in any mysql connection pooling or uwsgi complications. (Real reason - I've never used Lambda and it was a good chance to try ;)
 * Python – good for fast prototyping
 * RDS Aurora Serverless – I choose relative database, as it is more classic. When there are no insights on product future better to start with something well known in organization. Then switch to better suitable technology when details appear.

As for db schema:
```sql
CREATE TABLE `birthdays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `year` YEAR(4) NOT NULL,
  `month` TINYINT(2) UNSIGNED NOT NULL,
  `day` TINYINT(2) UNSIGNED NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`name`)
);

```
I would also store some excess data, which is not used for API calls. That could be a "preventive optimization", but it is easier to `drop column` then having no data for report or making decisions. At the start db size grow usually does not matter much.

Name seems to be bad UID and it likely would change in next versions. So adding `id` on the start. As for varchar, longest name I could think of is `Ostap-Suleyman-Berta-Maria-Bender-Bey` is only 38 chars. So, using 1byte prefix varchar of 255 chars should be enough.

# Workflow

Development should be easy for new team members. Only requirements for their workstation is `Docker` and `Git`. All dependencies (virtualenv) and utils are in container. This way whole team use same exact libs for development, and workstations are clean.

```bash
./run stack
```

This would start local development environment. Mock of `API gateway` and local `RDS` instance in container. 
Logs would appear on screen, to stop `API gateway` press `Ctrl-C`.

Now open new terminal and do:
```bash
./run bash
```
You are in virtualenv container and can make a test request:
```
curl localhost:3000/hello/John
```
Port `:3000` also forwarded, so it is possible to make requests from Host. Live edit is enabled for sources in `app/` folder.

When you are done with changes - test:
```bash
./run test style
```
This should be added to pre-commit hook ;)


```bash
./run test unit
```
This would run unit tests. Something to begin with at CI.

As for CD, example of canary deployment to dev is provided:
```bash
./run deploy dev
```
This would deploy new version of Lambda function, and then start to switch traffic to it at 10% increments each minute. CloudWatch alarms that are triggered by any errors raised by the deploymen also provided. They would automatically rollback the deployment.

```bash
$ ./run deploy dev
Successfully packaged artifacts and wrote output template to file tmp/packaged.yaml.
Execute the following command to deploy the packaged template
aws cloudformation deploy --template-file /Users/aryabov/Projects/hello/tmp/packaged.yaml --stack-name <YOUR STACK NAME>

Waiting for changeset to be created..
Waiting for stack create/update to complete
Successfully created/updated stack - sam-app

$ time curl  https://4ob5sizuk8.execute-api.eu-west-1.amazonaws.com/Prod/hello/John
{"message": "Hello, John! Your birthday is in 37 days"}
real    0m0.283s
user    0m0.032s
sys     0m0.018s
```
Stage prefix `/Prod/` is only untill publishing `API gateway` to custom domain.  
Versioning and aliasing helps SRE to rollback manually.  
Logs and tracedumps are prepended with `Request-Id`, but to use json or Splunk's `key=value` depends on organisation.   
Metrics could be exported from CloudWatch to your centralized monitoring.