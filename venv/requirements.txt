aws-sam-cli==0.7.0
awscli==1.16.60
flake8==3.6.0
mycli==1.19.0
PyMySQL==0.9.2
pytest-mock==1.10.0
pytest==4.0.1